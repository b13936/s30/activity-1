const express = require('express');
const app = express();
const PORT = 4000;
const mongoose = require('mongoose');

app.use(express.json());
app.use(express.urlencoded({extended: true}));


mongoose.connect('mongodb+srv://khrade:Kronos12@batch139.lgkts.mongodb.net/Users?retryWrites=true&w=majority', 
	{useNewUrlParser: true, useUnifiedTopology: true}
);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to database`));


const userSchema = new mongoose.Schema(
	{
		name: String,
		status: {
			type: String,
			default: "pending"
		}
	}
);

const User = mongoose.model("User", userSchema);

app.post("/signup", (req,res) => {


	User.findOne({name: req.body.name}, (err, result) => {console.log(result)

		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found")
		} else {
			let newUser = new User({
				name: req.body.name
			})

			newUser.save((err, savedUser) => {
				if(err){
					return console.error(err)
				} else {
					return res.send(`New User created`)
				}
			})
		}
	});

app.listen(PORT, () => console.log(`Server running at port ${PORT}`));